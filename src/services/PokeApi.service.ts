import axios from 'axios';
import { useState } from 'react';
import { ApiState, PokeApiData } from '../interface/Interface';

export function useGetPosts() {
  const [fetchState, setFetchState] = useState(ApiState.DEFAULT);
  const [posts, setPosts] = useState<Array<PokeApiData>>([]);

  const urlBase2 = 'https://pokeapi.co/api/v2/pokemon';

  const getPosts = async () => {
    try {
      setFetchState(ApiState.LOADING);

      const res = await axios.get(urlBase2);
      const resData = res.data.results as Array<PokeApiData>;

      setPosts(resData);
      console.log(resData);

      setFetchState(ApiState.SUCCESS);

      return resData;
    } catch (err) {
      setFetchState(ApiState.ERROR);
    }
  };

  return [posts, fetchState, getPosts] as const;
}