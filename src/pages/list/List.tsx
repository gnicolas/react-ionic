import { IonContent, IonPage, IonHeader} from '@ionic/react';
import { IonList, IonItem, IonLabel} from '@ionic/react';
import { IonCard, IonImg, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent } from '@ionic/react';
import { IonChip} from '@ionic/react';
import { useGetPosts } from '../../services/PokeApi.service';
import './List.css';
import axios from 'axios';

import Progress from './progress.gif'

import { ApiState, PokemonDetail } from '../../interface/Interface';
import { useEffect, useState } from 'react';

const List: React.FC = () => {

  const [post, fetchState, getPosts] = useGetPosts();

  const [pokemonDetail, setPokemonDetail] = useState<PokemonDetail>();

  const getItemData = async (pokemon: any) => {
      console.log(pokemon);

      const response = await axios.get(pokemon.url);

      const p: PokemonDetail = {
        id:  response.data.id,
        name: pokemon.name,
        img: response.data.sprites.front_default,
        type: response.data.types[0].type.name,
        moves: response.data.abilities
      }

      setPokemonDetail(p);
  };

  useEffect(() => {
    getPosts();
  }, []);

  return (
    <IonPage>
      <IonHeader>
            <div className='card-fixed'>

              <div className='container'>
                { pokemonDetail ? 

                    <IonCard id='card-list-pokemos' key={pokemonDetail.id} >

                        <IonCardHeader style={{textAlign: 'center'}}>
                          <IonImg id='img-list-pokemons' className="size" src={pokemonDetail.img}></IonImg> 
                          <IonCardSubtitle>
                            <IonChip color="success">
                              <IonLabel color="dark" >{pokemonDetail.type}</IonLabel>
                            </IonChip>  
                          </IonCardSubtitle> 
                          <IonCardTitle style={{fontWeight: 'bold'}}>{pokemonDetail.name}</IonCardTitle>
                        </IonCardHeader>

                        <IonCardContent className='text-center'>
                            <h3>Habilidades</h3>
                            {pokemonDetail.moves.map((move: any) =>
                              <IonChip key={move.slot} color="success">
                                <IonLabel color="dark">{move.ability.name}</IonLabel>
                              </IonChip>
                            )}
                        </IonCardContent>
                 
                      </IonCard> 
                    : null
                  }
              </div>
                
              </div>
          </IonHeader>
      <IonContent>

        <div className='list-pokemons'>

          {fetchState === ApiState.LOADING ? 
            <img className="icon-progress" src={Progress} alt="progress"/>: null
          }

          {fetchState === ApiState.ERROR ? 
            <div>
              Error Api Pokemon
            </div> : null
          }

          {fetchState === ApiState.SUCCESS ? 
            <div className='container'>
              <IonList>
                {post.map(pokemon =>
                  <IonItem key={pokemon.id} onClick={() => getItemData(pokemon)} >
                    <IonLabel>
                      <h2>{pokemon.name}</h2>
                    </IonLabel>
                  </IonItem>
                )}
              </IonList>
            </div> : null
          }
        </div>

      </IonContent>
    </IonPage>
  );
};

export default List;
