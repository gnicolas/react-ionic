import { IonContent, IonPage, IonHeader} from '@ionic/react';
import './Tienda.css';

import CardResponsive from '../../components/CardResponsive';

import NormalPotion from "./images/potions/normal-potion.png";
import SuperPotion from "./images/potions/super-potion.png";
import HyperPotion from "./images/potions/hyper-potion.png";
import MaximunPotion from "./images/potions/maximun-potion.png";

import Normal from "./images/pokeballs/normal.png";
import Super from "./images/pokeballs/super.png";
import Ultra from "./images/pokeballs/ultra.png";
import Master from "./images/pokeballs/master.png";
import Veloz from "./images/pokeballs/veloz.png";
import Gloria from "./images/pokeballs/gloria.png";

function createData(title: string, img: string, description: string, color: string) {
  return { title, img, description, color };
}

const typesPocions = [
  createData('Normal Pocion', NormalPotion, 'Potion', '#EBDEF0'),
  createData('Super Pocion', SuperPotion, 'Potion', '#FDEBD0'),
  createData('Hyper Pocion', HyperPotion, 'Potion', '#FADBD8'),
  createData('Maximum Pocion', MaximunPotion, 'Potion', '#D6EAF8')
];

const typesPokeballs = [
  createData('Normal', Normal, 'Pokeball', '#FADBD8'),
  createData('Super', Super, 'Pokeball', '#D4E6F1'),
  createData('Ultra', Ultra, 'Pokeball', '#D5D8DC'),
  createData('Veloz', Veloz, 'Pokeball', '#F9E79F'),
  createData('Gloria', Gloria, 'Pokeball', '#E6B0AA'),
  createData('Master', Master, 'Pokeball', '#D2B4DE'),
];

const Tienda: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
      </IonHeader>
      <IonContent>

        <div className='container'>

            <CardResponsive 
              groups={typesPocions} 
              cardHeight='333px' 
              colResponsive='col-sm-3' 
              rowMargin='55px'
              buttonsResponse='col-xs-3 col-md-12 col-lg-6'>
              </CardResponsive>

            <div className="row" >
              <div className="col-sm-6" style={{marginTop: '69px'}}>
                <a href='/testing' style={{textDecoration: 'none'}}>
                  <div className="card card-pokeball">
                    <div className="card-body bag">
                      <p className="card-text">MOCHILA</p>
                    </div>
                  </div>  
                </a>
                
              </div>
              <div className="col-sm-6" style={{marginTop: '69px', marginBottom: '69px'}}>
                <a href='/testing' style={{textDecoration: 'none'}}>
                  <div className="card card-pokeball">
                    <div className="card-body pokedex">
                    <p className="card-text">POKEDEX</p>
                    </div>
                  </div>  
                </a>
              </div>
            </div>
          
            <CardResponsive 
              groups={typesPokeballs} 
              cardHeight='369px' 
              colResponsive='col-sm-2' 
              rowMargin='55px'
              buttonsResponse='col-xs-3 col-md-12 col-lg-12'>
            </CardResponsive>

            <div className="row" >
              <div className="col-sm-12" style={{marginTop: '69px', marginBottom: '69px'}}>
                <div className="card card-pokeball">
                  <div className="card-body battle">
                  <h2 className="card-title text-pokeball" style={{fontWeight: 'bold'}}>Batallas Pokemon</h2>
                  </div>
                </div>  
              </div>
            </div>
       
        </div>
      
            
      </IonContent>
    </IonPage>
  );
};

export default Tienda;
