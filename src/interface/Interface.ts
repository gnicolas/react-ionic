export enum ApiState {
    DEFAULT = 'DEFAULT',
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    ERROR = 'ERROR',
}

export type PokeApiData = {
    id: number;
    name: string;
    url: string;
}

export type Pokemon = {
    id: number;
    name: string;
    sprites: {
        front_default: string;
    }
}

export type PokemonDetail = {
    id: number;
    name: string;
    img: string;
    type: string;
    moves: Array<string>;
}