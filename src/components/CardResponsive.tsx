import './CardResponsive.css';

interface ContainerCardGroup {
  title: string;
  img: string;
  description: string;
  color: string;
}

interface ContainerProps {
  groups: Array<ContainerCardGroup>;
  colResponsive: string;
  rowMargin: string;
  cardHeight: string;
  buttonsResponse: string;
}

const svgSearch = 
<svg width="24" height="24" viewBox="0 0 24 24">
    <path fill="currentColor" d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
</svg>

const svgDetails =
<svg width="24" height="24" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20 21H4V10H6V19H18V10H20V21M3 3H21V9H3V3M9.5 11H14.5C14.78 11 15 11.22 15 11.5V13H9V11.5C9 11.22 9.22 11 9.5 11M5 5V7H19V5H5Z" />
</svg>

const svgBuy =
<svg width="24" height="24" viewBox="0 0 24 24">
    <path fill="currentColor" d="M17,18C15.89,18 15,18.89 15,20A2,2 0 0,0 17,22A2,2 0 0,0 19,20C19,18.89 18.1,18 17,18M1,2V4H3L6.6,11.59L5.24,14.04C5.09,14.32 5,14.65 5,15A2,2 0 0,0 7,17H19V15H7.42A0.25,0.25 0 0,1 7.17,14.75C7.17,14.7 7.18,14.66 7.2,14.63L8.1,13H15.55C16.3,13 16.96,12.58 17.3,11.97L20.88,5.5C20.95,5.34 21,5.17 21,5A1,1 0 0,0 20,4H5.21L4.27,2M7,18C5.89,18 5,18.89 5,20A2,2 0 0,0 7,22A2,2 0 0,0 9,20C9,18.89 8.1,18 7,18Z" />
</svg>


const CardResponsive: React.FC<ContainerProps> = ({ groups, colResponsive, rowMargin, cardHeight, buttonsResponse }) => {
  return (
    <div className="row">
      {groups.map((group) => (   
          <div className={colResponsive} style={{marginTop: rowMargin, marginBottom: rowMargin}}>
              <div className="card" style={{background: group.color}}>
                <img src={group.img} className="icon-size" alt="..." />
                <div className="card-body card-responsive" style={{height: cardHeight}}>
                  <p className="card-text card-text text-center">{group.description}</p>
                  <h3 className="card-title card-text text-center">{group.title}</h3>

                  {
                    buttonsResponse ? 
                    <div className='container' style={{marginTop: '33px'}}>
                      <div className="row align" >
                        <div className={buttonsResponse}>
                          <div className='buttons align'>
                            {svgSearch}
                          </div>
                        </div>
                        <div className={buttonsResponse}>
                          <div className='buttons align'>
                              {svgDetails}
                          </div>
                        </div>

                        <div className={buttonsResponse} style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                          <div className='buttons align'>
                            {svgBuy}
                          </div>
                        </div>
                      </div>
                    </div> : null
                  }
                  
                </div>
              </div>
        </div>
      ))}
    </div>
  );
};

export default CardResponsive;
